﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

class convo
{
    public int Int;
    public float Float;
    public char Char;
    public double Double;
    public string str;

}

class tofloat : convo      // class that contains methods of type conversion of float. 
{
    public void convo(float a)                    //method that contains type conversion float.   
    {
        Console.WriteLine("below are the conversion of float type:");

        Console.WriteLine("float to int " + (int)a);                     

        Console.WriteLine("float to double " + (double)a);

    }
}

class toint:tofloat     //class contain method of type conversion of integer
{
    public void convo(int a)  
    {
        Console.WriteLine("below are the conversion of Integer type:");
        Float = a;                                                      //implicit conversion
        Console.WriteLine("integer to float " + Float);
        Double = a;                                                     //implicit conversion

        Console.WriteLine("integer to double " + Double);

        Char = Convert.ToChar(a);                                      //explict
        Console.WriteLine("Integer to char " + Char);
    }
}

class tochar : toint
{
    public void convo(char a)
    {
        Console.WriteLine("below are the conversion of char type:");
        Int = a;                                               //implicit
        Console.WriteLine("char to int" + Int);
        Double = a;                                            //implicit
        Console.WriteLine("char to double" + Double);
    }
}

class tostring: tochar    //class for convo of string 
{


    public void convo(string str)
    {
        Console.WriteLine("below are the conversion of string type:");

        Int = Convert.ToInt32(str);   // explicit

        
                                                             //implicit conversion

        Console.WriteLine("string to int " + Int);

       
       
    }


}

class task1 : tostring
{

}


//convo method will work as overloaded

namespace firstapp
{
    class Program
    {
        static void Main(string[] args)
        {
            task1 t1 = new task1();
            t1.convo("1234");
            Console.ReadKey();
        }
    }
}

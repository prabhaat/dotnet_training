﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task3
{
    class Program
    {
        static void Main(string[] args)
        {

            string s1 = "abcd";
            string s2 = s1; // both are pointing at same memory location containing abcd

            s1 = "bcvd"; //currently s1 is pointing at different memory location while s2 is pointing at abcd

            //another example which will show us that strings are references
            /*
             * we know that pointetr act as a refrence to the address of a variable
             * we know array is a pointer that act as reference to first element and other element are added in incremental order
             */

            //string as array

            string str = "hello";

            Console.WriteLine("array implementation printing first element" + str[0]);

            //this will not work with value type variable 


        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace task5
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.Write("Enter the Color: ");
            String color = Console.ReadLine();
            switch (color)
            {

                case "red":
                    Console.WriteLine("It is red");
                    break;

                case "blue":
                    Console.WriteLine("It is blue");
                    break;

                case "green":
                    Console.WriteLine("It is green");
                    break;
                default:
                    Console.WriteLine("switch to any other color type");
                    break;
            }


            Console.ReadKey();

        }
    }
}
